
> Vue component for adding a side menu with swipe opening (Vue компонент, который добавляет боковое меню с возможностью открытия оного свайпом)


## Props

`zIndex {Number} default=999` - CSS z-index of menu (css z-index меню)

`position {String} default="right"` - Position of menu, may be "right" or "left" (Позиция меню, может быть "right", справа, или "left", слева.)

`isMobileVer {Boolean} default=false` - Is mobile version or not. Need to use mouse events or touch events (Мобильная версия или нет. Необходимо для использования событий мыши или прикосновений)

`canOpen {Boolean} default=true` - Can open? It can be used to prohibit opening two side menus at the same time. (Можно ли открыть? Может использоваться для запрета на открытие одновременно двух боковых меню.)

`animationSpeed {Number} default=400` - Animation speed in ms (Скорость анимации в мс.)

`open {Number}` - Opens the menu when changed (При изменении открывает меню)

`close {Number}` - Closes the menu when changed (При изменении закрывает меню)

## Events

`open` - The menu has started opening (Меню начало открываться)

`show` - The menu is open (Меню уже открыто)

`close` - The menu has started closing (Меню начало закрываться)

`hide` - The menu is close (Меню уже закрыто)

## Usage

```html
template>
    <div id="app">
        <smart-side-menu position="right" :can-open="true" :is-mobile-ver="true">
            <!-- Your menu here -->
        </smart-side-menu>
    </div>
</template>

<script>
    import SmartSideMenu from "./pathToComponent/SmartSideMenu";
    export default {
        name: 'app',
        components: {SmartSideMenu},
    }
</script>
```

## License

MIT © [Dukentre](https://dukentre.ru/)
